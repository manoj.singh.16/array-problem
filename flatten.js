const flatten = (elements, depth = 1) => {
  // Flattens a nested array (the nesting can be to any depth).
  // Hint: You can solve this using recursion.
  // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];

  if (!elements || !Array.isArray(elements)) return [];

  const toReturn = [];

  for (let i = 0; i < elements.length; i++) {
    if (Array.isArray(elements[i]) && depth > 0) {
      toReturn.push(...flatten(elements[i], depth - 1));
    } else {
      toReturn.push(elements[i]);
    }
  }

  return toReturn;
};

module.exports = flatten;
