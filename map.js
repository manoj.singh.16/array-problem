const map = (elements, cb) => {
  // Do NOT use .map, to complete this function.
  // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
  // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
  // Return the new array.

  //added a check for so that it will only work for arrays, it will not work for strings.

  if (!cb || !elements || !Array.isArray(elements) || typeof cb !== "function")
    return [];

  const toReturn = [];
  for (let i = 0; i < elements.length; i++) {
    toReturn.push(cb(elements[i], i, elements));
  }
  return toReturn;
};

module.exports = map;
