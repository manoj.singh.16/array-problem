const { array } = require("../array");
const each = require("../each");

const cb = (ele, i) => console.log(ele, i);

const result = each(array, cb);
