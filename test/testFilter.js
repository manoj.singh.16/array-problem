const { array } = require("../array");
const filter = require("../filter");

const cb = (ele) => ele % 2 === 0;

const result = filter(array, cb);

console.log(result);
