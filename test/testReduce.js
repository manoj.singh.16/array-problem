const { array } = require("../array");
const reduce = require("../reduce");

const cb = (acc, val) => acc * val;

const result = reduce(array, cb, 2);

console.log(result);
