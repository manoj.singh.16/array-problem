const { array } = require("../array");
const map = require("../map");

const cb = (ele, i, arr) => ele * 2;

const result = map(array, cb);

console.log(result);
