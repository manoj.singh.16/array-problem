const { nestedArray } = require("../array");
const flatten = require("../flatten");

const result = flatten(nestedArray, 3);

console.log(result);
