const filter = (elements, cb) => {
  // Do NOT use .filter, to complete this function.
  // Similar to `find` but you will return an array of all elements that passed the truth test
  // Return an empty array if no elements pass the truth test

  if (!cb || !elements || !Array.isArray(elements) || typeof cb !== "function")
    return [];

  let toReturn = [];

  for (let i = 0; i < elements.length; i++) {
    if (cb(elements[i], i, elements)) {
      toReturn.push(elements[i]);
    }
  }
  return toReturn;
};

module.exports = filter;
