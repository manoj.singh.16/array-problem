let items = [1, 2, 3, 4, 5, 5];

const nestedArray = [1, [2], [[3]], [[[4]]]]; // use this to test 'flatten'

module.exports.array = items;
module.exports.nestedArray = nestedArray;
